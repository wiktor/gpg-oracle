# GPG Oracle

Decrypts and signs data submitted to it.

This is primarily used by the Thunderbird experiment.

## Prerequisites

This application needs two other services running:

  - Certificate Store: https://gitlab.com/wiktor/minics Set `CERT_STORE_ROOT` to `http://localhost:3000`
  - Private Key Store: https://gitlab.com/wiktor/pks-agent/ and a target backend: https://gitlab.com/sequoia-pgp/pks-openpgp-card set `PKS_AGENT_SOCK` to `/run/user/1000/pks-agent.sock`

## Endpoints

There are two endpoints:

  - `/decrypt` - any data that is POSTed will be decrypted an an unencrypted PGP message will be returned (why not plaintext? Because Thunderbird expects unwrapped PGP message),
  - `/sign/{key}` - creates a detached signature using given `{key}`. The key is fetched from the certificates store.

## Thunderbird integration

  1. Go through the initial build instructions: https://developer.thunderbird.net/thunderbird-development/getting-started
  2. Apply the patch in `comm` directory: `hg patch --no-commit thunderbird-comm-pks.patch`
  3. Start the prerequisites and this application (e.g. via `cargo run`)
  4. Configure OpenPGP encryption in Thunderbird and toggle the `mail.openpgp.allow_external_gnupg` switch to `true`
  5. Enjoy decryption and signing provided by the PKS agent!
