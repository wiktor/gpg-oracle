use actix_web::{error, web, App, Error, HttpRequest, HttpResponse, HttpServer};
use futures::StreamExt;
use openpgp::policy::StandardPolicy;

use std::io::{self, Write};

use openpgp::cert::prelude::*;
use openpgp::crypto::SessionKey;
use openpgp::parse::{stream::*, Parse};
use openpgp::types::SymmetricAlgorithm;
use sequoia_openpgp as openpgp;

const MAX_SIZE: usize = 1_262_144; // max payload size is 256k

async fn decrypt(mut payload: web::Payload) -> Result<HttpResponse, Error> {
    // payload is a stream of Bytes objects
    let mut body = web::BytesMut::new();
    while let Some(chunk) = payload.next().await {
        let chunk = chunk?;
        // limit max size of in-memory payload
        if (body.len() + chunk.len()) > MAX_SIZE {
            return Err(error::ErrorBadRequest("overflow"));
        }
        body.extend_from_slice(&chunk);
    }

    let helper = Helper {};

    // Now, create a decryptor with a helper using the given Certs.
    let policy = &StandardPolicy::new();
    let mut decryptor = DecryptorBuilder::from_bytes(&body)
        .unwrap()
        .with_policy(policy, None, helper)
        .unwrap();

    // Decrypt the data.
    let mut plaintext = vec![];
    io::copy(&mut decryptor, &mut plaintext)?;
    //

    use openpgp::serialize::stream::{LiteralWriter, Message};

    let mut sink = vec![];
    let message = Message::new(&mut sink);
    let mut w = LiteralWriter::new(message).build().unwrap();
    w.write_all(&plaintext).unwrap();
    w.finalize().unwrap();

    let mut writer = openpgp::armor::Writer::new(vec![], openpgp::armor::Kind::Message).unwrap();
    writer.write_all(&sink).unwrap();
    let buffer = writer.finalize().unwrap();

    Ok(HttpResponse::Ok().content_type("text/plain").body(buffer))
}

async fn sign(req: HttpRequest, mut payload: web::Payload) -> Result<HttpResponse, Error> {
    // payload is a stream of Bytes objects
    let mut body = web::BytesMut::new();
    while let Some(chunk) = payload.next().await {
        let chunk = chunk.unwrap();
        // limit max size of in-memory payload
        if (body.len() + chunk.len()) > MAX_SIZE {
            return Err(error::ErrorBadRequest("overflow"));
        }
        body.extend_from_slice(&chunk);
    }

    let name = req.match_info().get("name").unwrap_or("World");
    eprintln!("Sign <{}>", name);

    let client = reqwest::blocking::Client::new();
    let mut signing_key = None;

    let policy = &openpgp::policy::StandardPolicy::new();
    for cert in CertParser::from_bytes(
        &client
            .get(format!(
                "{}/{}",
                std::env::var("CERT_STORE_ROOT").expect("CERT_STORE_ROOT is set"),
                name
            ))
            .send()
            .unwrap()
            .bytes()
            .unwrap(),
    )
    .unwrap()
    .flatten()
    {
        if let Some(key) = cert
            .keys()
            .with_policy(policy, None)
            .supported()
            .alive()
            .revoked(false)
            .for_signing()
            .next()
        {
            signing_key = Some(key.key().clone());
            break;
        }
    }
    let signing_keypair = gpg_oracle::unlock_signer(
        std::env::var("PKS_AGENT_SOCK").expect("PKS_AGENT_SOCK is set"),
        signing_key.unwrap(),
        &String::new().into(),
    )
    .unwrap();
    let mut sink = vec![];
    use openpgp::serialize::stream::Armorer;
    use openpgp::serialize::stream::{Message, Signer};
    let message = Message::new(&mut sink);
    let message = Armorer::new(message)
        .kind(openpgp::armor::Kind::Signature)
        .build()
        .unwrap();
    let mut signer = Signer::new(message, signing_keypair)
        .detached()
        .build()
        .unwrap();

    let mut input = std::io::Cursor::new(&body);

    std::io::copy(&mut input, &mut signer).unwrap();
    signer.finalize().unwrap();

    Ok(HttpResponse::Ok().content_type("text/plain").body(sink))
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    use std::os::unix::io::FromRawFd;

    let fds = std::env::var("LISTEN_FDS").unwrap();
    eprintln!("FDS: {}", fds);

    let fds: i32 = fds.parse().unwrap();
    //let listener = unsafe { std::os::unix::net::UnixListener::from_raw_fd(fds + 2) };
    let listener = unsafe { std::net::TcpListener::from_raw_fd(fds + 2) };
    listener.set_nonblocking(true).unwrap();

    HttpServer::new(move || {
        App::new()
            .route("/decrypt", web::post().to(decrypt))
            .route("/sign/{name}", web::post().to(sign))
    })
    //        .listen_uds(listener)?
    .listen(listener)?
    //.bind(("127.0.0.1", 8412))?
    //.bind_uds("/run/user/1000/pks-dummy.sock")?
    .run()
    .await
} //

struct Helper;

impl VerificationHelper for Helper {
    fn get_certs(&mut self, _ids: &[openpgp::KeyHandle]) -> openpgp::Result<Vec<openpgp::Cert>> {
        Ok(Vec::new())
    }

    fn check(&mut self, _structure: MessageStructure) -> openpgp::Result<()> {
        Ok(())
    }
}

impl DecryptionHelper for Helper {
    fn decrypt<D>(
        &mut self,
        pkesks: &[openpgp::packet::PKESK],
        _skesks: &[openpgp::packet::SKESK],
        sym_algo: Option<SymmetricAlgorithm>,
        mut decrypt: D,
    ) -> openpgp::Result<Option<openpgp::Fingerprint>>
    where
        D: FnMut(SymmetricAlgorithm, &SessionKey) -> bool,
    {
        let client = reqwest::blocking::Client::new();
        for pkesk in pkesks {
            let keyid = pkesk.recipient();

            let key = CertParser::from_bytes(
                &client
                    .get(format!(
                        "{}/{:X}",
                        std::env::var("CERT_STORE_ROOT").expect("CERT_STORE_ROOT is set"),
                        keyid
                    ))
                    .send()?
                    .bytes()?,
            )?
            .filter_map(|result| result.ok())
            .flat_map(|cert| {
                cert.keys()
                    .filter(|key| key.keyid() == *keyid)
                    .map(|key| key.key().clone())
                    .collect::<Vec<_>>()
            })
            .next();
            if let Some(key) = key {
                eprintln!("Trying key {}...", key.keyid());
                if let Ok(mut decryptor) = gpg_oracle::unlock_decryptor(
                    std::env::var("PKS_AGENT_SOCK").expect("PKS_AGENT_SOCK is set"),
                    key.clone(),
                    &String::new().into(),
                ) {
                    eprintln!("Key unlocked! {}", key.keyid());

                    pkesk
                        .decrypt(decryptor.as_mut(), sym_algo)
                        .map(|(algo, session_key)| decrypt(algo, &session_key));
                }
            }
        }

        Ok(None)
    }
}
